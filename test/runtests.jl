using Graphs
using GraphSpectra
using SparseArrays
using Test

const testdir = dirname(@__FILE__)

tests = [
	"ElectricalNetworks/ElectricalNetworks"
]

@testset "GraphSpectra" begin
	for t in tests
		tp = joinpath(testdir, "$t.jl")
		include(tp)
	end
end
