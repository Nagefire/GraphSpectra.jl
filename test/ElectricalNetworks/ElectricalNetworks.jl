@testset "ElectricalNetworks" begin
	@testset "rdist" begin
		n = rand(10:300)
		i = rand(1:n)
		j = rand(1:n)

		A = adjacency_matrix(path_graph(n))
		@test isapprox(rdist(A, i, j), abs(j-i))

		A = adjacency_matrix(complete_graph(n))
		@test isapprox(rdist(A, i, j), 2/n)

		A = adjacency_matrix(star_graph(n))
		@test isapprox(rdist(A, 1, i), 1)
	end

	@testset "kconst" begin
		n = rand(10:400)

		A = adjacency_matrix(complete_graph(n))
		@test isapprox(kconst(A), (n-1)^2/n)

		A = adjacency_matrix(cycle_graph(n))
		@test isapprox(kconst(A), (n+1)*(n-1)/6)

		A = adjacency_matrix(path_graph(n))
		@test isapprox(kconst(A), (n-1)^2/3 + 1/6)

		A = adjacency_matrix(star_graph(n))
		@test isapprox(kconst(A), n-3/2)
	end

	@testset "kind" begin
		n = rand(10:400)

		A = adjacency_matrix(path_graph(n))
		@test isapprox(kind(A), n*(n-1)*(n+1)/6)

		A = adjacency_matrix(complete_graph(n))
		@test isapprox(kind(A), n-1)

		A = adjacency_matrix(cycle_graph(n))
		@test isapprox(kind(A), n*(n-1)*(n+1)/12)

		A = adjacency_matrix(star_graph(n))
		@test isapprox(kind(A), (n-1)^2)
	end
end
