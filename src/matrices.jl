export degree_matrix, laplacian_matrix, normalized_adjacency,
       normalized_laplacian, transition_matrix, incidence_matrix

degree_matrix(A::AbstractMatrix)=Diagonal(A*ones(size(A, 1)))

# Define graph matrices, in order of most -> least common
"""
    laplacian_matrix(A[, signed=true])

Return the (signed) laplacian matrix of the graph.
"""
laplacian_matrix(A::AbstractMatrix; signed::Bool=true)=
    if signed
        degree_matrix(A) - A
    else
        degree_matrix(A) + A
    end

@doc raw"""
    normalized_adjacency(A)

Return the normalized adjacency matrix ``\mathcal A = D^{-1/2} A
D^{-1/2}`` of the graph.
"""
function normalized_adjacency(A::AbstractSparseMatrix{<:Real})
    D = Diagonal([1/√d for d in A*ones(size(A, 1))])

    D*A*D
end

@doc raw"""
    normalized_laplacian(A)

Return the normalized laplacian matrix ``I - \mathcal A = D^{-1/2} L
D^{-1/2}`` of the graph.
"""
normalized_laplacian(A::AbstractSparseMatrix)=I-normalized_adjacency(A)

@doc raw"""
    transition_matrix(A)

Return the transition matrix ``P`` of the graph.

This is the transition matrix for a random walk on the vertices of the
graph, with entries
```math
    P(i, j) = \begin{cases}
        1/d_i &\text{if } i \sim j \\
        0 &\text{otherwise}.
    \end{cases}
```
"""
function transition_matrix(A::AbstractSparseMatrix)
    D = Diagonal([1/d for d in A*ones(size(A, 1))])

    D*A
end

"""
    incidence_matrix(A[, directed=true])

Return the (undirected) incidence matrix of `A`.
"""
incidence_matrix(A::AbstractSparseMatrix; directed::Bool=true)=
    if directed
        _dir_incidence(A)
    else
        _undir_incidence(A)
    end

function _undir_incidence(A::AbstractSparseMatrix{T}) where T <: Real
    n, m = nv(A), ne(A)
    E = edges(A)
    rows = zeros(Int, 2m)
    cols = zeros(Int, 2m)
    vals = ones((T <: Integer) ? Int8 : T, 2m)

    # Weighted/nonweighted cases
    if T <: Integer
        ind = 1

        for j=1:m
            u, v = E[j]

            rows[ind] = u
            rows[ind+1] = v
            cols[ind] = j
            cols[ind+1] = j

            ind += 2
        end
    else
        ind = 1

        for j=1:m
            u, v = E[j]

            rows[ind] = u
            rows[ind+1] = v
            cols[ind] = j
            cols[ind+1] = j
            vals[ind] = vals[ind+1] = sqrt(A[u, v])

            ind += 2
        end
    end

    sparse(rows, cols, vals, n, m)
end

function _dir_incidence(A::AbstractSparseMatrix{T}) where T <: Real
    n, m = nv(A), ne(A)
    E = edges(A)
    rows = zeros(Int, 2m)
    cols = zeros(Int, 2m)
    vals = ones((T <: Integer) ? Int8 : T, 2m)

    # Weighted/nonweighted cases
    if T <: Integer
        ind = 1

        for j=1:m
            u, v = E[j]

            rows[ind] = u
            rows[ind+1] = v
            cols[ind] = cols[ind+1] = j
            vals[ind+1] = -1

            ind += 2
        end
    else
        ind = 1

        for j=1:m
            u, v = E[j]
            val = sqrt(A[u, v])

            rows[ind] = u
            rows[ind+1] = v
            cols[ind] = cols[ind+1] = j
            vals[ind+1] = -val

            ind += 2
        end
    end

    sparse(rows, cols, vals, n, m)
end
