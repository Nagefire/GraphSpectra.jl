export adjacency_spectrum, laplacian_spectrum, normalized_spectrum,
       normalized_laplacian_spectrum, algebraic_connectivity

"""
    adjacency_spectrum(A, k=3[, return_vectors=false])

Return the `k` eigenvalues of greatest magnitude of the adjacency
matrix.

# Arguments
- `A::AbstractSparseMatrix{<:Real}` - the adjacency matrix of the
  graph.
- `k::Integer=3` - the number of eigenvalues to compute.
- `return_vectors::Bool=false` - if true, returns eigenvectors.
"""
function adjacency_spectrum(A::AbstractSparseMatrix{<:Real},
                            k::Integer=3; return_vectors::Bool=false)
    # Check preconditions
    @assert(issymmetric(A), "A must be symmetric")

    n = size(A, 1)

    @assert(0 < k <= n, "nev must be at least 1 and no greater "
                      + "than $n")

    if k == n
        λ, v = eigs(A; nev=n-1, which=:LM)

        push!(λ, -sum(λ))

        if return_vectors λ, v else λ end
    else
        λ, v = eigs(A; nev=k, which=:LM)

        if return_vectors λ, v else λ end
    end
end

"""
    laplacian_spectrum(A, k=3[, return_vectors=false])

Return the `k` least eigenvalues of the graph Laplacian.

# Arguments
- `A::AbstractSparseMatrix{<:Real}` - the adjacency matrix of the
  graph.
- `k::Integer=3` - the number of eigenvalues to compute.
- `return_vectors::Bool=false` - if true, returns eigenvectors.
"""
function laplacian_spectrum(A::AbstractSparseMatrix{<:Real},
                            k::Integer=3; return_vectors::Bool=false)
    n = size(A, 1)

    @assert(0 < k <= n, "k must be at least 1 and no greater "
                        + "than $n")

    L = laplacian_matrix(A)

    if k == n
        λ, v = eigs(L; nev=n-1, which=:LR)

        push!(λ, 0)

        if return_vectors reverse(λ), v else reverse(λ) end
    else
        λ, v = eigs(L; nev=k, which=:SR)

        if return_vectors λ, v else λ end
    end
end

@doc raw"""
    algebraic_connectivity(A[, return_vector=false])

Return the algebraic connectivity of the graph represented by the
adjacency matrix.
"""
function algebraic_connectivity(A::AbstractSparseMatrix{<:Real};
                                return_vector::Bool=false)
    L = laplacian_matrix(A)

    λ, v = eigs(L; nev=2, which=:SR)

    if return_vector λ[2], v[:, 2] else λ[2] end
end

@doc raw"""
    normalized_spectrum(A, k=3[, return_vectors=false])

Return the `k` least normalized eigenvalues of the graph. The
normalized eigenvalues coincide with the eigenvalues of the transition
matrix due to the similarity ``P = D^{-1/2} \mathcal A D^{1/2}``.

# Arguments
- `A::AbstractSparseMatrix{<:Real}` - the adjacency matrix of the
  graph.
- `k::Integer=3` - the number of eigenvalues to compute.
- `return_vectors::Bool=false` - if true, returns eigenvectors.
"""
function normalized_spectrum(A::AbstractSparseMatrix{<:Real},
                             k::Integer=3; return_vectors::Bool=false)
    # Verify preconditions
    𝒜 = normalized_adjacency(A)
    n = size(A, 1)

    @assert(0 < k <= n, "nev must be at least 1 and no greater "
                      + "than $n")

    if k == n
        λ, v = eigs(𝒜  ; nev=n-1, which=:SR)

        push!(λ, -sum(λ))
        # TODO: compute the final eigenvector

        if return_vectors λ, v else λ end
    else
        λ, v = eigs(𝒜  ; nev=k, which=:SR)

        if return_vectors λ, v else λ end
    end
end

"""
    normalized_laplacian_spectrum(A, k=3, [return_vectors=false])

Return the `k` least eigenvalues of the normalized laplacian of the
graph.

# Arguments
- `A::AbstractSparseMatrix{<:Real}` - the adjacency matrix of the
  graph.
- `k::Integer=3` - the number of eigenvalues to compute.
- `return_vectors::Bool=false` - if true, returns eigenvectors.
"""
function normalized_laplacian_spectrum(A::AbstractSparseMatrix{<:Real},
                                       k::Integer=3;
                                       return_vectors::Bool=false)
    ℒ = normalized_laplacian(A)
    n = size(A, 1)

    @assert(0 < k <= n, "nev must be at least 1 and no greater "
                          + "than $n")

    if k == n
        λ, v = eigs(ℒ; nev=n-1, which=:SR)

        push!(λ, n-sum(λ))

        if return_vectors λ, v else λ end
    else
        λ, v = eigs(ℒ; nev=k, which=:SR)

        if return_vectors λ, v else λ end
    end
end
