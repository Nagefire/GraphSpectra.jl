export trees, forests

"""
    trees(A)

Return the number of spanning trees in `A`.

!!! Warning
For dense graphs or those with several vertices, this function may
throw an `InexactError`.
"""
function trees(A::AbstractSparseMatrix{<:Real})
    n = size(A, 1)

    L = laplacian_matrix(A)
    L = @inbounds L[1:(n-1), 1:(n-1)]

    trunc(Int, det(L))
end

@doc raw"""
    forests(A, v₁...)

Compute the number of spanning n-forests separating ``v_1, v_2, \ldots,
v_n`` in `A`.
"""
function forests(A::AbstractSparseMatrix{<:Real}, v::Integer...)
    n = size(A, 1)
    k = size(v, 1)

    @assert(k > 1, "There must be at least two vertices to create a"
                 * " spanning forest")
    @assert(k < n, "G has order $n but $k vertices were passed")

    if !allunique(i for i in v)
        return 0
    end

    L = laplacian_matrix(A)
    slice = fill(true, n)

    for i in v
        @inbounds slice[i] = false
    end

    trunc(Int, det(@inbounds L[slice, slice]))
end
