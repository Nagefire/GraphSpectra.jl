"""
    GraphSpectra.ElectricalNetworks

Functions for computing invariants of electrical networks.
"""
module ElectricalNetworks

# External dependencies
import IterativeSolvers: minres!

import LinearAlgebra: Symmetric, issymmetric, dot, norm

import SparseArrays: AbstractSparseMatrix, AbstractSparseVector, det,
                     tril, sparsevec

# Internal dependencies
import ..GraphSpectra: laplacian_spectrum, laplacian_matrix,
                       normalized_laplacian_spectrum

# Spanning trees and spanning forests
include("forests.jl")

export rdist, kconst, kind, trees, forests

"""
    kconst(A)

Compute the Kemeny's constant of `A`.
"""
function kconst(A::AbstractSparseMatrix{<:Real})
    n = size(A, 1)
    λ = normalized_laplacian_spectrum(A, n)

    sum(x -> 1/x, λ[2:end])
end

"""
    kind(A)

Compute the Kirchhoff index of `A`.
"""
function kind(A::AbstractSparseMatrix{<:Real})
    n = size(A, 1)
    λ = laplacian_spectrum(A, n)

    n*sum(x -> 1/x, λ[2:end])
end

"""
    rdist(A, i, j)

Compute the effective resistance between `i` and `j`.

# Arguments
- `A::AbstractSparseMatrix{<:Real}` - the adjacency matrix of the
  graph.
- `i::Integer, j::Integer` - the indices of the vertex pair.
"""
function rdist(A::AbstractSparseMatrix{<:Real},
               i::Integer, j::Integer)
    @assert(issymmetric(A), "A must be symmetric")

    n = size(A, 1)

    @assert(1 <= i <= n, "i must be a positive integer less than $n")
    @assert(1 <= j <= n, "j must be a positive integer less than $n")

    i == j && return 0.0
    L = laplacian_matrix(A)
    e = sparsevec([i, j], [1., -1.], n)
    x = zeros(n)
    sol = minres!(x, L, e; initially_zero=true)

    sol[i] - sol[j]
end

end # ElectricalNetworks
