module GraphSpectra

import Arpack: eigs

import LinearAlgebra: Diagonal, I, issymmetric

import SparseArrays: AbstractSparseMatrix, findnz, nnz, sparse

# Basic functions
include("matrices.jl")
include("spectra.jl")

# Submodules
include("ElectricalNetworks/ElectricalNetworks.jl")
using .ElectricalNetworks

export # Graph matrices
       laplacian_matrix, normalized_adjacency, normalized_laplacian,
       transition_matrix, incidence_matrix,
       # Graph spectra
       adjacency_spectrum, laplacian_spectrum, normalized_spectrum,
       normalized_laplacian_spectrum, algebraic_connectivity,
       # Electrical network invariants
       rdist, kconst, kind, trees, forests

end # module
