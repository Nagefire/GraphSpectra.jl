using Documenter
using GraphSpectra

makedocs(
	modules = [GraphSpectra],
	format = Documenter.HTML(),
	sitename = "GraphSpectra",
	doctest = false,
	pages = Any[
		"Home" => "index.md",
		"Core" => "core.md",
	]
)

deploydocs(
	repo = "gitlab.com/nagefire/graphspectra.jl",
	target = "build"
)
