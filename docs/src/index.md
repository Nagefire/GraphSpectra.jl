# GraphSpectra
_GraphSpectra.jl_ is a lightweight package that implements spectral
algorithms and heuristics for spectral analysis of graphs. This module
is stand-alone, but integration with types from
[Graphs.jl](https://github.com/JuliaGraphs/Graphs.jl) is planned and
currently in-progress. The default routines operate on sparse adjacency
list representations of graphs.

Sparse adjacency matrices are usually represented as `SparseMatrixCSC`,
but routines are implemented for `AbstractSparseMatrix`.

## Inspiration
This project was inspired by the code from
- [Laplacians.jl](https://github.com/danspielman/Laplacians.jl)
- [ResistanceDistance.jl](https://gitlab.com/byudm/resistancedistance.jl)
- [Nonbacktracking.jl](https://gitlab.com/byudm/nonbacktracking.jl)
and many others. _Laplacians.jl_ features many great algorithms, but
has been out of active development for a while and does not conform
to the Julia style guide. Many of the routines for spectral
sparsification will be transferred from this package and modified to
have more Julian syntax.

## Goals
This package is designed specifically for analysis of graph spectra.
The core of _GraphSpectra.jl_ provides performant methods for
computing graph eigenvalues. The current implementation relies on
routines from [Arpack.jl](https://arpack.julialinearalgebra.org/stable/)
and [IterativeSolvers.jl](https://iterativesolvers.julialinearalgebra.org/dev/)
to complete this function. We have planned implementation of improved
eigenvalue solvers with the [LOBPCG](https://wikimili.com/en/LOBPCG)
method and minimal-stretch spanning trees.

Our secondary purpose is to provide performant implementations of
useful spectral algorithms, such as
- Counting spanning trees via products/determinants
- Determining topological indices of graphs
- Clustering
- Graph sparsification
- Graph flows
- Graph drawing
