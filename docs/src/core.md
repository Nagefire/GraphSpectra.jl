# Core Functions
The core functions of _GraphSpectra.jl_ compute graph matrices and
their spectra.

## Graph Matrices
```@docs
GraphSpectra.laplacian_matrix
GraphSpectra.normalized_adjacency
GraphSpectra.normalized_laplacian
GraphSpectra.transition_matrix
GraphSpectra.incidence_matrix
```

## Graph Spectra
```@docs
GraphSpectra.adjacency_spectrum
GraphSpectra.algebraic_connectivity
GraphSpectra.laplacian_spectrum
GraphSpectra.normalized_spectrum
GraphSpectra.normalized_laplacian_spectrum
```
